import {ADD_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT, FETCH_USERS_SUCCESS, FIND_PRODUCT} from "../constants/product.constants";

export const defaultProductState = [
    {
    id: "a", 
    name: 'Razer Deathader',
    price: 120,
    img: 'https://vitebsk.sila.by/img/catalog2015/ntpk/tovar75396.jpg',
    description: "Игровая мышь, разработанная в соответствии со строгими требованиями элиты киберспорта, сочетает передовые технологии с потрясающей эргономикой и повышенной прочностью. Мышь Razer DeathAdder Elite оснащена самым передовым в мире оптическим сенсором, совершенно новыми механическими переключателями мыши от Razer и безупречной эргономикой, которой безгранично доверяют лучшие профессиональные киберспортсмены по всему миру. С ней вы обретете непревзойденную точность и скорость для доминирования в игре."
    },
    {
    id: "b", 
    name: 'Razer Nagа Hex',
    price: 95,
    img: 'https://4play.by/upload/iblock/d6e/d6e694a4fa0ae209ef2c4d51bf3389a4.jpg',
    description: "Razer Nagа Hex - разработана некоторыми из лучших в мире инженеров по эргономике. Эта игровая мышь доказала свою способность свести к  минимуму усталость во время продолжительных периодов игры дома или в соревновательных турнирах. Вместо 12 кнопочной геймерской MMO мышки Razer Naga, Razer Naga Hex имеет 6 больших кнопок для большого пальца, оптимизированных специально для пользовательских интерфейсов МОВА и экшен-RPG."
    },
    {
    id: "c", 
    name: 'Razer Basilisk Ultimate',
    price: 210,
    img: 'https://4play.by/upload/resize_cache/iblock/0b8/720_600_1/0b87555eb08044a061a4fc94dc530e4a.png',
    description: "С Razer Basilisk Ultimate победа будет полностью в ваших руках. Эту высокопроизводительную беспроводную игровую мышь можно настроить так, чтобы она выглядела, играла и чувствовалась именно так, как хочется именно вам. У ваших соперников нет никаких шансов. Razer HYPERSPEED на 25% быстрее чем любая другая существующая беспроводная технология, вы даже не осознаете, что играете беспроводной мышью. "
    }
]

export const  productReducer = (state = defaultProductState, action) => {
    switch (action.type) {
        case FIND_PRODUCT: 
            return state.filter(product => product.name.toLowerCase() === action.data.toLowerCase()); 
        case ADD_PRODUCT:
            return [...state, action.data];
        case DELETE_PRODUCT:
            return state.filter((product) => product.id !== action.id)
        case FETCH_USERS_SUCCESS:
            return [...state, ...action.data];          
        case EDIT_PRODUCT:
            let newState = [];
            state.forEach((product) => {
            if(product.id === action.data.id) {
                newState.push(action.data)
            } else {
                newState.push(product)
            }});
            return newState; 
        default:
            return state;
    }
};  