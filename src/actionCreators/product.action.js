import { ADD_PRODUCT, EDIT_PRODUCT, DELETE_PRODUCT, FIND_PRODUCT } from "../constants/product.constants";

export const addProduct = (data) => ({type: ADD_PRODUCT, data});
export const editProduct = (data) => ({type: EDIT_PRODUCT, data});
export const deleteProduct = (id) => ({type: DELETE_PRODUCT, id});
export const searchProduct = (data) => ({type: FIND_PRODUCT, data});