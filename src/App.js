import React, { useReducer } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Navbar from "./components/Navbar";

import UserProfile from "./components/profilePage";
import UserPage from "./components/userPage";
import AdminPage from "./components/adminPage";

import Context from "./utils/context";
import * as PRODUCTACTIONS from "./actionCreators/product.action";
import { productReducer, defaultProductState } from "./reducers/product.reducer";

import * as USERACTIONS from "./actionCreators/profile.action";
import { profileReducer, defaultUserState} from "./reducers/profile.reducer"

function App() {
  const [stateProduct, dispatchProductReducer] = useReducer(productReducer, defaultProductState);
  const addProduct = (data) => {
    dispatchProductReducer(PRODUCTACTIONS.addProduct(data));
  };
  const searchProduct = (data) => {
    dispatchProductReducer(PRODUCTACTIONS.searchProduct(data));
  };
  const editProduct = (data) => {
    dispatchProductReducer(PRODUCTACTIONS.editProduct(data));
  }
  const deleteProduct = (data) => {
    dispatchProductReducer(PRODUCTACTIONS.deleteProduct(data));
  }

  const [stateUser, dispatchUsertReducer] = useReducer(profileReducer, defaultUserState)
  const editUser = (data) => {
    dispatchUsertReducer(USERACTIONS.editUser(data));
  }

  return (
      <BrowserRouter>
        <Navbar />
        <Context.Provider
          value =
          {{
            productState: stateProduct,
            addProduct: (data) => addProduct(data),
            searchProduct: (data) => searchProduct(data),
            editProduct: (data) => editProduct(data),
            deleteProduct: (data) => deleteProduct(data)
          }}
        >
          <Route path="/adminPage" component={AdminPage} />
          <Route path='/userPage' component={UserPage} />
        </Context.Provider>

        <Context.Provider
          value = 
          {{
            userState: stateUser,
            editUser: (data) => editUser(data)
          }}
        >
        <Route path='/userProfile' component={UserProfile} />
        </Context.Provider>

      </BrowserRouter>

  );
}

export default App;
