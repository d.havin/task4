import {getUsersQuery} from "../services/user.service";
import {fetchUsersSuccess} from "../actionCreators/product.action";


export const getAllUsers = () => (dispatch) => {
    getUsersQuery()
        .then((response) => response.json())
        .then((data) => {
            console.log(data)
            dispatch(fetchUsersSuccess(data));
        })
        .catch((err) => {
            console.log(err)
        })
};
