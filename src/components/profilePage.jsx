import React, {useState, useContext} from 'react';
import '../styles/profilePage.css';
import Context from '../utils/context';

// вынести импут . отсоеденить от редакса. состояния инпутов в компоненте профайл
// дочерний компонент инпута
// в инпуте условный рендеринг (проверка на длинну введенных данных)

function ProfileInfo(props) {
    const [editedName, setName] = useState('');
    const [editedSurName, setSurName] = useState('');
    const [EditedCartInfo, setCartInfo] = useState('');
    const context = useContext(Context)
    
    const handleEdit = () => {
        let user = {
            name : editedName? editedName: context.userState.name,
            surName: editedSurName? editedSurName: context.userState.surName,
            cartInfo: EditedCartInfo? EditedCartInfo: context.userState.cartInfo
        }
        context.editUser(user)
    
        setName('');
        setSurName('');
        setCartInfo('');
    }

    return (
        <div className = "editInfoMenu">

            <h2 className="editUserTitle">Изменить данные</h2>
            <div className="userInfo"> Данные пользователя: {context.userState.name} {context.userState.surName} {context.userState.cartInfo}</div>
            Имя <input value={editedName} type = "text" placeholder = {context.userState.name} onChange={(e) => setName(e.target.value)}/>
            Фамилия <input value={editedSurName} type = "text" placeholder = {context.userState.surName} onChange={(e) => setSurName(e.target.value)}/>
            Карта <input value={EditedCartInfo} type = "text" placeholder = {context.userState.cartInfo} onChange={(e) => setCartInfo(e.target.value)}/>
            <button onClick={handleEdit}>Изменить</button>
            
        </div>
    )
}

export default ProfileInfo
